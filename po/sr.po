# Serbian translation for redshift.
# Copyright (C) 2016 redshift's COPYRIGHT HOLDER
# This file is distributed under the same license as the redshift package.
# Марко М. Костић (Marko M. Kostić), 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-09-04 12:22-0700\n"
"PO-Revision-Date: 2016-09-20 09:54+0000\n"
"Last-Translator: Марко М. Костић <Unknown>\n"
"Language-Team: \n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:1
#, fuzzy
msgid ""
"Adjusts the color temperature of your screen according to your surroundings. "
"This may help your eyes hurt less if you are working in front of the screen "
"at night."
msgstr ""
"Редшифт мења температуру боје вашег екрана у складу са вашим окружењем. Ово "
"вам може помоћи уколико осећате бол у очима када радите за рачунаром ноћу."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:2
msgid ""
"The color temperature is set according to the position of the sun. A "
"different color temperature is set during night and daytime. During twilight "
"and early morning, the color temperature transitions smoothly from night to "
"daytime temperature to allow your eyes to slowly adapt."
msgstr ""
"Температура боје се се подешава у складу са положајем сунца. Другачија "
"температура боје се поставља ноћу а другачија дању. У сумрак и рано јутро "
"температура боје лагано прелази из ноћне у дневну температуру да би се вашим "
"очима омогућило лакше привикавање."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:3
#, fuzzy
msgid ""
"This program provides a status icon that allows the user to control color "
"temperature."
msgstr ""
"Овај програм пружа иконицу стања која омогућава кориснику да управља "
"Редшифтом."

#: ../data/applications/gammastep.desktop.in.h:1
msgid "gammastep"
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:2
msgid "Color temperature adjustment"
msgstr "Прилагођавање температуре боје"

#: ../data/applications/gammastep.desktop.in.h:3
msgid "Color temperature adjustment tool"
msgstr "Алатка за прилагођавање температуре боје"

#: ../data/applications/gammastep-indicator.desktop.in.h:1
msgid "Gammastep Indicator"
msgstr ""

#: ../data/applications/gammastep-indicator.desktop.in.h:2
#, fuzzy
msgid "Indicator for color temperature adjustment"
msgstr "Прилагођавање температуре боје"

#. TRANSLATORS: Name printed when period of day is unknown
#: ../src/redshift.c:92
msgid "None"
msgstr "Непознато"

#: ../src/redshift.c:93
msgid "Daytime"
msgstr "Дан"

#: ../src/redshift.c:94 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1022
msgid "Night"
msgstr "Ноћ"

#: ../src/redshift.c:95
msgid "Transition"
msgstr "Прелаз"

#: ../src/redshift.c:178 ../src/gammastep_indicator/statusicon.py:313
#: ../src/gammastep_indicator/statusicon.py:326
msgid "Period"
msgstr "Период"

#: ../src/redshift.c:188 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1018
#, fuzzy
msgid "Day"
msgstr "Дан"

#. TRANSLATORS: Abbreviation for `north'
#: ../src/redshift.c:198
msgid "N"
msgstr "С"

#. TRANSLATORS: Abbreviation for `south'
#: ../src/redshift.c:200
msgid "S"
msgstr "Ј"

#. TRANSLATORS: Abbreviation for `east'
#: ../src/redshift.c:202
msgid "E"
msgstr "И"

#. TRANSLATORS: Abbreviation for `west'
#: ../src/redshift.c:204
msgid "W"
msgstr "З"

#: ../src/redshift.c:206 ../src/gammastep_indicator/statusicon.py:319
msgid "Location"
msgstr "Место"

#: ../src/redshift.c:280
#, fuzzy
msgid "Failed to initialize provider"
msgstr "Нисам успео да покренем достављача %s.\n"

#: ../src/redshift.c:294 ../src/redshift.c:334 ../src/redshift.c:378
#: ../src/redshift.c:404
#, fuzzy
msgid "Failed to set option"
msgstr "Нисам успео да подесим опцију %s.\n"

#: ../src/redshift.c:297 ../src/redshift.c:336
msgid "For more information, use option:"
msgstr ""

#: ../src/redshift.c:324 ../src/redshift.c:395
#, fuzzy
msgid "Failed to parse option"
msgstr "Нисам успео да обрадим опцију „%s“.\n"

#: ../src/redshift.c:349
#, fuzzy
msgid "Failed to start provider"
msgstr "Нисам успео да покренем достављача %s.\n"

#: ../src/redshift.c:364
#, fuzzy
msgid "Failed to initialize method"
msgstr "Доступни начини прилагођавања:\n"

#: ../src/redshift.c:379 ../src/redshift.c:405
msgid "For more information, try:"
msgstr ""

#: ../src/redshift.c:417
#, fuzzy
msgid "Failed to start adjustment method"
msgstr "Доступни начини прилагођавања:\n"

#: ../src/redshift.c:445
msgid "Latitude must be in range"
msgstr ""

#: ../src/redshift.c:452
msgid "Longitude must be in range:"
msgstr ""

#: ../src/redshift.c:479 ../src/redshift.c:497 ../src/redshift.c:624
#: ../src/redshift.c:1096
#, fuzzy
msgid "Unable to read system time."
msgstr "Не могу да прочитам системско време.\n"

#: ../src/redshift.c:567
msgid "Waiting for initial location to become available..."
msgstr ""

#: ../src/redshift.c:572 ../src/redshift.c:763 ../src/redshift.c:777
#: ../src/redshift.c:1081
#, fuzzy
msgid "Unable to get location from provider."
msgstr "Доступни достављачи локација:\n"

#: ../src/redshift.c:577 ../src/redshift.c:800
#, fuzzy
msgid "Invalid location returned from provider."
msgstr "Доступни достављачи локација:\n"

#: ../src/redshift.c:584 ../src/redshift.c:715 ../src/redshift.c:1130
#: ../src/redshift.c:1155 ../src/gammastep_indicator/statusicon.py:307
#: ../src/gammastep_indicator/statusicon.py:325
msgid "Color temperature"
msgstr "Температура боје"

#: ../src/redshift.c:585 ../src/redshift.c:718 ../src/redshift.c:1003
#: ../src/redshift.c:1131
#, fuzzy
msgid "Brightness"
msgstr "Осветљење: %.2f\n"

#: ../src/redshift.c:614
#, fuzzy
msgid "Status"
msgstr "Стање: %s\n"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:302
msgid "Disabled"
msgstr "Онемогућено"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:61
#: ../src/gammastep_indicator/statusicon.py:302
msgid "Enabled"
msgstr "Омогућено"

#: ../src/redshift.c:727 ../src/redshift.c:1139 ../src/redshift.c:1163
#: ../src/redshift.c:1184
#, fuzzy
msgid "Temperature adjustment failed."
msgstr "Прилагођавање температуре није успело.\n"

#: ../src/redshift.c:783
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available..."
msgstr ""

#: ../src/redshift.c:890
msgid "Partial time-configuration unsupported!"
msgstr ""

#: ../src/redshift.c:897
msgid "Invalid dawn/dusk time configuration!"
msgstr ""

#: ../src/redshift.c:926
#, fuzzy
msgid "Trying location provider"
msgstr "Покушавам са достављачем локације „%s“...\n"

#: ../src/redshift.c:931
#, fuzzy
msgid "Trying next provider..."
msgstr "Покушавам са следећим достављачем...\n"

#: ../src/redshift.c:936
#, fuzzy
msgid "Using provider:"
msgstr "Користим достављача „%s“.\n"

#: ../src/redshift.c:944
#, fuzzy
msgid "No more location providers to try."
msgstr "Испробао сам све достављаче локација.\n"

#: ../src/redshift.c:952
msgid ""
"High transition elevation cannot be lower than the low transition elevation."
msgstr ""

#: ../src/redshift.c:958
msgid "Solar elevations"
msgstr ""

#: ../src/redshift.c:965
#, fuzzy
msgid "Temperatures"
msgstr "Температура: %i\n"

#: ../src/redshift.c:975 ../src/redshift.c:986
#, fuzzy
msgid "Temperature must be in range"
msgstr "Прилагођавање температуре није успело.\n"

#: ../src/redshift.c:997
msgid "Brightness must be in range"
msgstr ""

#: ../src/redshift.c:1010
msgid "Gamma value must be in range"
msgstr ""

#: ../src/redshift.c:1018 ../src/redshift.c:1022
msgid "Gamma"
msgstr ""

#: ../src/redshift.c:1049
#, fuzzy
msgid "Trying next method..."
msgstr "Покушавам са следећим достављачем...\n"

#: ../src/redshift.c:1054
#, fuzzy
msgid "Using method"
msgstr "Користим достављача „%s“.\n"

#: ../src/redshift.c:1061
#, fuzzy
msgid "No more methods to try."
msgstr "Испробао сам све достављаче локација.\n"

#: ../src/redshift.c:1075
msgid "Waiting for current location to become available..."
msgstr ""

#: ../src/redshift.c:1086
#, fuzzy
msgid "Invalid location from provider."
msgstr "Доступни достављачи локација:\n"

#: ../src/redshift.c:1112
msgid "Solar elevation"
msgstr ""

#: ../src/redshift.c:1147 ../src/redshift.c:1171 ../src/redshift.c:1192
#, fuzzy
msgid "Press ctrl-c to stop..."
msgstr "Притисните ctrl-c за престанак рада...\n"

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: ../src/options.c:144
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "Коришћење: %s -l ГЕО.ШИРИНА:ДУЖИНА -t ДАН:НОЋ [ОПЦИЈЕ...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: ../src/options.c:150
msgid "Set color temperature of display according to time of day.\n"
msgstr "Подеси температуру боје екрана у складу са тренутним временом.\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: ../src/options.c:156
#, fuzzy
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tIncrease logging verbosity\n"
"  -q\t\tDecrease logging verbosity\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tПриказује ову помоћну поруку\n"
"  -v\t\tПричљив излаз\n"
"  -V\t\tПриказује издање програма\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: ../src/options.c:165
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""

#. TRANSLATORS: help output 5
#: ../src/options.c:187
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""

#. TRANSLATORS: help output 6
#: ../src/options.c:196
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"Подразумеване вредности:\n"
"\n"
"  Дневна температура: %uK\n"
"  Ноћна температура: %uK\n"

#. TRANSLATORS: help output 7
#: ../src/options.c:204
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "Пријављујте грешке у програму на <%s>\n"

#: ../src/options.c:211
msgid "Available adjustment methods:\n"
msgstr "Доступни начини прилагођавања:\n"

#: ../src/options.c:217
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr "Наведите опције раздвојене цртицом са „-m НАЧИН:ОПЦИЈЕ“.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:220
msgid "Try `-m METHOD:help' for help.\n"
msgstr "Укуцајте `-m НАЧИН:help' за више података.\n"

#: ../src/options.c:227
msgid "Available location providers:\n"
msgstr "Доступни достављачи локација:\n"

#: ../src/options.c:233
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr "Наведите опције раздвојене цртицом са „-l ДОСТАВЉАЧ:ОПЦИЈЕ“.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:236
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "Укуцајте `-l ДОСТАВЉАЧ:help' за више података.\n"

#: ../src/options.c:337
msgid "Malformed gamma argument.\n"
msgstr "Деформисана вредност гама аргумента.\n"

#: ../src/options.c:338 ../src/options.c:450 ../src/options.c:473
msgid "Try `-h' for more information.\n"
msgstr ""

#: ../src/options.c:387 ../src/options.c:586
#, fuzzy
msgid "Unknown location provider"
msgstr "Покушавам са достављачем локације „%s“...\n"

#: ../src/options.c:419 ../src/options.c:576
#, fuzzy
msgid "Unknown adjustment method"
msgstr "Доступни начини прилагођавања:\n"

#: ../src/options.c:449
msgid "Malformed temperature argument.\n"
msgstr ""

#: ../src/options.c:545 ../src/options.c:557 ../src/options.c:566
msgid "Malformed gamma setting.\n"
msgstr ""

#: ../src/options.c:596
msgid "Malformed dawn-time setting"
msgstr ""

#: ../src/options.c:606
msgid "Malformed dusk-time setting"
msgstr ""

#: ../src/options.c:612
msgid "Unknown configuration setting"
msgstr ""

#: ../src/config-ini.c:143
msgid "Malformed section header in config file.\n"
msgstr ""

#: ../src/config-ini.c:179
msgid "Malformed assignment in config file.\n"
msgstr ""

#: ../src/config-ini.c:190
msgid "Assignment outside section in config file.\n"
msgstr ""

#: ../src/gamma-drm.c:86
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr ""

#: ../src/gamma-drm.c:94
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr ""

#: ../src/gamma-drm.c:104 ../src/gamma-randr.c:373
#, c-format
msgid "CRTC %d does not exist. "
msgstr ""

#: ../src/gamma-drm.c:107 ../src/gamma-randr.c:376
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr ""

#: ../src/gamma-drm.c:110 ../src/gamma-randr.c:379
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr ""

#: ../src/gamma-drm.c:148
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr ""

#: ../src/gamma-drm.c:154
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""

#: ../src/gamma-drm.c:167
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""

#: ../src/gamma-drm.c:231
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr ""

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: ../src/gamma-drm.c:236
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""

#: ../src/gamma-drm.c:249
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr ""

#: ../src/gamma-drm.c:253 ../src/gamma-randr.c:358 ../src/gamma-vidmode.c:150
#: ../src/gamma-dummy.c:56
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr ""

#: ../src/gamma-wl.c:70
msgid "Not authorized to bind the wlroots gamma control manager interface."
msgstr ""

#: ../src/gamma-wl.c:90
msgid "Failed to allocate memory"
msgstr ""

#: ../src/gamma-wl.c:124
msgid "The zwlr_gamma_control_manager_v1 was removed"
msgstr ""

#: ../src/gamma-wl.c:177
msgid "Could not connect to wayland display, exiting."
msgstr ""

#: ../src/gamma-wl.c:218
msgid "Ignoring Wayland connection error while waiting to disconnect"
msgstr ""

#: ../src/gamma-wl.c:247
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr ""

#: ../src/gamma-wl.c:282
msgid "Wayland connection experienced a fatal error"
msgstr ""

#: ../src/gamma-wl.c:347
msgid "Zero outputs support gamma adjustment."
msgstr ""

#: ../src/gamma-wl.c:352
msgid "output(s) do not support gamma adjustment"
msgstr ""

#: ../src/gamma-randr.c:83 ../src/gamma-randr.c:142 ../src/gamma-randr.c:181
#: ../src/gamma-randr.c:207 ../src/gamma-randr.c:264 ../src/gamma-randr.c:424
#, c-format
msgid "`%s' returned error %d\n"
msgstr ""

#: ../src/gamma-randr.c:92
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr ""

#: ../src/gamma-randr.c:127
#, c-format
msgid "Screen %i could not be found.\n"
msgstr ""

#: ../src/gamma-randr.c:193 ../src/gamma-vidmode.c:85
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr ""

#: ../src/gamma-randr.c:266
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr ""

#: ../src/gamma-randr.c:290
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr ""

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: ../src/gamma-randr.c:295
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""

#: ../src/gamma-randr.c:317
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr ""

#: ../src/gamma-randr.c:353 ../src/gamma-vidmode.c:145
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""

#: ../src/gamma-vidmode.c:50 ../src/gamma-vidmode.c:70
#: ../src/gamma-vidmode.c:79 ../src/gamma-vidmode.c:106
#: ../src/gamma-vidmode.c:169 ../src/gamma-vidmode.c:214
#, c-format
msgid "X request failed: %s\n"
msgstr ""

#: ../src/gamma-vidmode.c:129
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr ""

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: ../src/gamma-vidmode.c:134
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr ""

#: ../src/gamma-dummy.c:32
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""

#: ../src/gamma-dummy.c:49
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr ""

#: ../src/gamma-dummy.c:64
#, c-format
msgid "Temperature: %i\n"
msgstr "Температура: %i\n"

#: ../src/location-geoclue2.c:49
#, c-format
msgid ""
"Access to the current location was denied!\n"
"Ensure location services are enabled and access by this program is "
"permitted.\n"
msgstr ""

#: ../src/location-geoclue2.c:95
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:138
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:176
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:217
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr ""

#: ../src/location-geoclue2.c:241
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr "Не могу да покренем GeoClue клијента: %s.\n"

#: ../src/location-geoclue2.c:380
msgid "GeoClue2 provider is not installed!"
msgstr ""

#: ../src/location-geoclue2.c:387
#, fuzzy
msgid "Failed to start GeoClue2 provider!"
msgstr "Нисам успео да покренем достављача %s.\n"

#: ../src/location-geoclue2.c:422
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr ""

#: ../src/location-geoclue2.c:431 ../src/location-manual.c:96
msgid "Unknown method parameter"
msgstr ""

#: ../src/location-manual.c:49
msgid "Latitude and longitude must be set."
msgstr ""

#: ../src/location-manual.c:65
msgid "Specify location manually.\n"
msgstr ""

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: ../src/location-manual.c:70
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""

#: ../src/location-manual.c:73
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""

#: ../src/location-manual.c:87
msgid "Malformed argument.\n"
msgstr "Деформисан аргумент.\n"

#: ../src/gammastep_indicator/statusicon.py:66
msgid "Suspend for"
msgstr "Обустави на"

#: ../src/gammastep_indicator/statusicon.py:68
msgid "30 minutes"
msgstr "30 минута"

#: ../src/gammastep_indicator/statusicon.py:69
msgid "1 hour"
msgstr "1 сат"

#: ../src/gammastep_indicator/statusicon.py:70
msgid "2 hours"
msgstr "2 сата"

#: ../src/gammastep_indicator/statusicon.py:71
#, fuzzy
msgid "4 hours"
msgstr "2 сата"

#: ../src/gammastep_indicator/statusicon.py:72
#, fuzzy
msgid "8 hours"
msgstr "2 сата"

#: ../src/gammastep_indicator/statusicon.py:81
msgid "Autostart"
msgstr "Самопокретање"

#: ../src/gammastep_indicator/statusicon.py:93
#: ../src/gammastep_indicator/statusicon.py:103
msgid "Info"
msgstr "Подаци"

#: ../src/gammastep_indicator/statusicon.py:98
msgid "Quit"
msgstr "Изађи"

#: ../src/gammastep_indicator/statusicon.py:136
msgid "Close"
msgstr "Затвори"

#: ../src/gammastep_indicator/statusicon.py:301
msgid "<b>Status:</b> {}"
msgstr "<b>Стање:</b> {}"

#: ../src/gammastep_indicator/statusicon.py:349
msgid "For help output, please run:"
msgstr ""

#, fuzzy, c-format
#~ msgid "Color temperature: %uK"
#~ msgstr "Температура боје: %uK\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f"
#~ msgstr "Осветљење: %.2f\n"

#, fuzzy, c-format
#~ msgid "Temperatures: %dK at day, %dK at night"
#~ msgstr "Температуре: %dK дању, %dK ноћу\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f:%.2f"
#~ msgstr "Осветљење: %.2f:%.2f\n"

#, fuzzy, c-format
#~ msgid "Period: %s"
#~ msgstr "Период: %s\n"

#, fuzzy, c-format
#~ msgid "Period: %s (%.2f%% day)"
#~ msgstr "Период: %s (%.2f%% дан)\n"

#, fuzzy, c-format
#~ msgid "Initialization of %s failed."
#~ msgstr "Покретање ставке „%s“ није успело.\n"

#, fuzzy, c-format
#~ msgid "Try `-m %s:help' for more information."
#~ msgstr "Укуцајте `-m НАЧИН:help' за више података.\n"

#, fuzzy
#~ msgid "redshift"
#~ msgstr "Редшифт"

#, c-format
#~ msgid "Location: %.2f %s, %.2f %s\n"
#~ msgstr "Место: %.2f %s, %.2f %s\n"

#~ msgid ""
#~ "The Redshift information window overlaid with an example of the redness "
#~ "effect"
#~ msgstr ""
#~ "Прозор са подацима Редшифта који је прекриван примером ефекта црвенила."
